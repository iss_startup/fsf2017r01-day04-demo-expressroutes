// Load modules required by the application
// Loads/imports the Express module and assigns it to a variable called express
var express = require('express');
// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// app is assigned an instance of express (express here refers to the variable express)
var app = express();

// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.NODE_PORT || 3000;

// Define paths
// __dirname is a global that holds the directory name of the current module
const CLIENT_FOLDER = path.join(__dirname + '/../client');  // CLIENT FOLDER is the public directory
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');




app.use(function(req, res, next){
    console.log('\nThis middleware gets called ALWAYS');
    next();
});

// Serves files from the public directory (CLIENT_FOLDER)
// __dirname is the absolute path of the application directory
app.use(express.static(CLIENT_FOLDER));

app.get('/a', function(req, res){
    console.log('/a: route terminated');
    res.send('You requested route /a');
});

app.get('/a', function(req, res){
    console.log('/a: This will never get called');
});

app.get('/a2', function(req, res){
    console.log('/a2: Text sent is HTML formatted');
    res.send('<h1>/a2: HTML format</h1>');
});

app.get('/b', function(req, res, next){
    console.log('/b: Route not terminated');
    next();
});

app.use(function(req, res, next){
    console.log('This middleware gets called SOMETIMES');
    next();
});

app.get('/b', function(req, res, next){
    console.log('/b (Part 2): Error thrown' );
    throw new Error('/b: Route failed');
});

app.use('/b', function(err, req, res, next){
    console.log('/b: Error detected. Handling error by passing it on');
    next(err);
});

app.get('/c', function(err, req){
    console.log('/c: Error thrown');
    throw new Error('C Failed');
});

app.use('/c', function(err, req, res, next){
    console.log('/c: Error detected but not passed on');
    next();
});

// Error handler: server error
app.use(function(err, req, res, next){
    console.error("Unhandled Error Detected: " + err);
    res.status(500).json({status:500, message: 'internal error', type:'internal'});
});

// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function(req, res){
    console.log('Route not handled');
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

app.listen(NODE_PORT, function(){
    console.log('Web server started on port %s. Ctrl-C to terminate.', NODE_PORT);
    console.log('See how routing works by trying these URLs and checking your console and browser:');
    console.log('(1) localhost:3000');
    console.log('(2) localhost:3000/a');
    console.log('(3) localhost:3000/a2');
    console.log('(4) localhost:3000/b');
    console.log('(5) localhost:3000/c');
    console.log('(6) localhost:3000/foo');
});